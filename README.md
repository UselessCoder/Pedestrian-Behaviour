﻿# Pedestrian-Behaviour

Simulaciónes de VR en dispositivos Oculus (compatibles) para

evaluar el comportamiento de peatones


## Actual state
Comportamiento de peatones: [Demo 1 (basics)](https://github.com/RicardoGuevara/Pedestrian-Behaviour/tree/master/comportamiento_peatones/demo/emergencia)

Evacuaciones: [Preeliminares](https://github.com/RicardoGuevara/Pedestrian-Behaviour/tree/master/evacuaciones/vrproyect)


## Sections
[Comportamiento de peatones](https://github.com/RicardoGuevara/Pedestrian-Behaviour/tree/master/comportamiento_peatones/demo/)


[Evacuaciones](https://github.com/RicardoGuevara/Pedestrian-Behaviour/tree/master/evacuaciones)


[Documentación](https://github.com/RicardoGuevara/Pedestrian-Behaviour/tree/master/Documentos)



## IT

[Unity 5.6.3f0 (64 bits)](https://unity3d.com/es/get-unity/download?thank-you=update&download_nid=47820&os=Win)


## Compatibility
Google cardboard

Samsung Gear-VR

Any android 5.0 (lollipop) or grater devices 

Oculus
## APK and resources

[APK](https://github.com/RicardoGuevara/Pedestrian-Behaviour/releases/download/v1.4.0/vr1.4.0.apk)

## Actual release:

<!--div align="center">
  
  <p>
    <img align="left" src="https://github.com/RicardoGuevara/Pedestrian-Behaviour/blob/master/imagenes/vista1.png" width="400" />  <h1>Demo 1: fase de pruebas </h1>
    <lu align="left">
      <li>Pulir escenario</li>
      <li>Agregar texturas al puente</li>
      <li>Agregar menus</li>
      <li>Configurar variables de entorno</li>
    </lu>
  </p>
  
</div-->

<div align="center">
  <p>
    <img align="center" src="https://github.com/RicardoGuevara/Pedestrian-Behaviour/blob/master/imagenes/pre-release-demo.PNG" width="800" />  
  </p>
  
</div>

